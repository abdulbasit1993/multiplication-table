package com.abdulbasitmehtab.multiplicationtable;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText number;
    Button submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        number = (EditText) findViewById(R.id.etNum);
        submit = (Button) findViewById(R.id.btSubmit);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String value = number.getText().toString();
                int num = Integer.parseInt(value);
                Intent in = new Intent(getApplicationContext(), SecondActivity.class);
                in.putExtra("num",num);
                startActivity(in);
            }
        });
    }
}
