package com.abdulbasitmehtab.multiplicationtable;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    TextView tv, tvTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        tv = (TextView) findViewById(R.id.tvResult);
        tvTitle = (TextView) findViewById(R.id.tvTitle);

        Intent myIntent = getIntent();
        int num = myIntent.getIntExtra("num",0);

        tvTitle.setText("Multiplication Table of "+num);

        for(int i = 1; i <= 10; i++)
        {
            tv.append("\n" + num + " x " + i + " = " + num*i);
        }
    }
}